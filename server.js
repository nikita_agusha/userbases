const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config")
const user = require("./app/user")

const app = express();
const PORT = 8000;


app.use(cors());
app.use(express.static("public"));
app.use(express.json())

mongoose.connect(`${config.db.url}/${config.db.name}`, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex:true
})
    .then(() => {
        console.log("mongoose connected!");
        app.use("/users", user());
        app.listen(PORT, () => {
            console.log("Server started at http://localhost:" + PORT);
        });
    });