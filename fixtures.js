const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User")

mongoose.connect(`${config.db.url}/${config.db.name}`);
const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("user");
    } catch (e) {
        console.log("collection  were not  presented. Skipping drop ...");
    }
    const [nikita, admin, user] = await User.create({
        username: "nikita",
        password: "123",
        phone:"123",
        email: "nikita@mail.ru",
        id:{
            number:"123"
        }
    }, {
        username: "admin",
        password: "123",
        phone:"123",
        email: "admin@mail.ru",
        id: {
            email:"admin@mail.ru"
        }
    }, {
        username: "user",
        password: "123",
        phone:"123",
        email: "user@mail.ru",
        id: {
            email:"user@mail.ru"
        }
    })

      db.close()
})