var jwt = require('jsonwebtoken');
const express = require("express");
const {nanoid} = require("nanoid");
const User = require("../models/User");

const createRouter = () => {
    const router = express.Router();
    router.get("/info", async (req, res) => {
        const token = req.get("Token");
        console.log(token)
        const user = await User.find({token: token})
        console.log(user[0].userId)
        try {
            res.send({id: user[0].userId});
        } catch (e) {
            res.status(500).send(e)
        }
    });

    router.post("/signup", async (req, res) => {
        const user = new User(req.body);
        user.token = nanoid();

        user.userId = req.body.phone
        try {
            await user.save();
            res.send({token: user.token});
        } catch (e) {
            res.status(400).send(e)
        }

    });

    router.post("/signin", async (req, res) => {
        console.log(req.body)
        const user = await User.findOne({userId: req.body.id});
        console.log(user)
        const token = jwt.sign({email :user.email}, "Stack", {

            expiresIn: '1m' // expires in 365 days

        });

        if (!user) {
            return res.status(400).send({error: "Username not found"});
        }


        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(400).send({error: "Wrong pass word"});
        }

        user.token = token;

        await user.save();

        res.send({user});
    });

    router.delete("/sessions", async (req, res) => {
        const token = req.get("Token");
        console.log(token)
        const success = {message: "Success"};

        if (!token) return res.send(success);
        console.log(token)
        console.log("asd")

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.token = "";

        await user.save()

        res.send(success);

    });

    return router;
};

module.exports = createRouter;